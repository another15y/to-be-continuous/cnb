## [2.4.2](https://gitlab.com/to-be-continuous/cnb/compare/2.4.1...2.4.2) (2024-07-01)


### Bug Fixes

* **Trivy:** Trivy 0.53.0 added the clean subcommand for semantic cache management ([f9dc8f1](https://gitlab.com/to-be-continuous/cnb/commit/f9dc8f1c3ec5844136338e96dcb5a63e7c9b5449))

## [2.4.1](https://gitlab.com/to-be-continuous/cnb/compare/2.4.0...2.4.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([44a62c8](https://gitlab.com/to-be-continuous/cnb/commit/44a62c8f8f010af9b2af6f5f4203ba304e2c98dc))

# [2.4.0](https://gitlab.com/to-be-continuous/cnb/compare/2.3.0...2.4.0) (2024-04-19)


### Features

* **Trivy:** add trivy-scan job ([904bf69](https://gitlab.com/to-be-continuous/cnb/commit/904bf69f41ede61ec2b94a29d11152850669554a))

# [2.3.0](https://gitlab.com/to-be-continuous/cnb/compare/2.2.1...2.3.0) (2024-1-27)


### Features

* migrate to CI/CD component ([7478d35](https://gitlab.com/to-be-continuous/cnb/commit/7478d35513e2e94bc22a520464bfd54d74572ea0))

## [2.2.1](https://gitlab.com/to-be-continuous/cnb/compare/2.2.0...2.2.1) (2023-12-22)


### Bug Fixes

* **publish:** propagated output vars ([3d66033](https://gitlab.com/to-be-continuous/cnb/commit/3d66033d0a9177adcb7523e39de42576bfcb0308))

# [2.2.0](https://gitlab.com/to-be-continuous/cnb/compare/2.1.2...2.2.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([e7abf48](https://gitlab.com/to-be-continuous/cnb/commit/e7abf48acc883a05cff0e85f5a538273c58f43b3))

## [2.1.2](https://gitlab.com/to-be-continuous/cnb/compare/2.1.1...2.1.2) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([cc35b41](https://gitlab.com/to-be-continuous/cnb/commit/cc35b41c30c7d145217c8c4dcdc6622c29e2e40e))

## [2.1.1](https://gitlab.com/to-be-continuous/cnb/compare/2.1.0...2.1.1) (2023-08-01)


### Bug Fixes

* failure while decoding a secret [@url](https://gitlab.com/url)@ does not cause the job to fail (warning message) ([f9fc0b2](https://gitlab.com/to-be-continuous/cnb/commit/f9fc0b200a20da6c244f0f81c330ae57c5d3a798))

# [2.1.0](https://gitlab.com/to-be-continuous/cnb/compare/2.0.0...2.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([f622dcd](https://gitlab.com/to-be-continuous/cnb/commit/f622dcdef897abb35db237a126fde30c4f0f60ac))

# [2.0.0](https://gitlab.com/to-be-continuous/cnb/compare/1.0.3...2.0.0) (2023-04-05)


### Features

* **publish:** redesign publish on prod strategy ([4acbdec](https://gitlab.com/to-be-continuous/cnb/commit/4acbdec71c9e1ddcfb502fd1885fd45bf5b83ded))


### BREAKING CHANGES

* **publish:** $PUBLISH_ON_PROD no longer supported (replaced by $CNB_PROD_PUBLISH_STRATEGY - see doc)

## [1.0.3](https://gitlab.com/to-be-continuous/cnb/compare/1.0.2...1.0.3) (2023-03-22)


### Bug Fixes

* use /workspace dir to avoid unwanted hierarchy in created image ([e28797c](https://gitlab.com/to-be-continuous/cnb/commit/e28797c0f3d14a0506b5fd1e72ae7300b0530626))

## [1.0.2](https://gitlab.com/to-be-continuous/cnb/compare/1.0.1...1.0.2) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([14fba12](https://gitlab.com/to-be-continuous/cnb/commit/14fba124571a09ca22a84e1fd25d522f3e2462b3))

## [1.0.1](https://gitlab.com/to-be-continuous/cnb/compare/1.0.0...1.0.1) (2022-09-28)


### Bug Fixes

* correct split of cnb_image when registry port is present ([bfe7372](https://gitlab.com/to-be-continuous/cnb/commit/bfe7372f8c1f50b494a6a31874099ef13e4e18fe))

# 1.0.0 (2022-08-31)


### Features

* initial release ([6628608](https://gitlab.com/to-be-continuous/cnb/commit/6628608431e5e7e06b3ea7b6a6b1fa45953c9907))
